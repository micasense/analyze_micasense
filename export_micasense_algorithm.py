import processing
import matplotlib.pyplot as plt
import math
from osgeo import gdal
from PyQt5.QtCore import QSettings, QTranslator, qVersion
from PyQt5.QtGui import QIcon, QColor, QIntValidator
from PyQt5.QtWidgets import (QAction, QFileDialog, QDialog, QLineEdit,
                             QDialogButtonBox, QFormLayout, QGroupBox)
import os.path
import numpy as np
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterDefinition,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterCrs,
                       QgsProcessingParameterMultipleLayers,
                       QgsProcessingParameterMatrix)

from qgis.core import (
    QgsRasterLayer,
    QgsProject,
    QgsSingleBandPseudoColorRenderer,
    QgsColorRampShader,
    QgsRasterShader,
    QgsRasterBandStats,
    QgsRasterResampler,
    QgsBilinearRasterResampler,
    QgsCubicRasterResampler,
    QgsContrastEnhancement,
    QgsLayerTreeLayer, QgsRasterPipe, QgsRasterFileWriter,
    QgsCoordinateReferenceSystem, QgsProcessingException)

class ExportMicaSenseAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector rlayer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'
    PROJECTION = "PROJECTION"
    TABLE = "TABLE"
    CLEAN = "CLEAN FILES"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        #input raster layer
        info = "Select Input Rasters"
        self.addParameter(QgsProcessingParameterMultipleLayers(
            self.INPUT,
            self.tr(info),
            QgsProcessing.TypeRaster))
        #Allows multiple rasters to be selected
    
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT,
                self.tr('Output folder')))
        #sets output directory as existing folder direcotry

        clean_files = (QgsProcessingParameterBoolean(self.CLEAN,
                                    self.tr("Removes exported rasters"),
                                    defaultValue=True))

        clean_files.setFlags(clean_files.flags() | 
                           QgsProcessingParameterDefinition.FlagAdvanced)

        self.addParameter(clean_files)
        #This param controls the deletion of output op's and compressed op's


    def processAlgorithm(self, parameters, context, feedback):
        
        """This algorithm takes a symoblised raster(s) from within qgis, exports
        them as an image , compresses said image, then tiles the compressed
        image for web tiling purposes"""


        input_rasters = self.parameterAsLayerList(parameters,
                                                  self.INPUT,
                                                  context)
                              
        output_dir = self.parameterAsFileOutput(parameters, 
                                                self.OUTPUT,
                                                context)

        remove_files = self.parameterAsBoolean(parameters,
                                               self.CLEAN,
                                               context)                                  
        if os.path.exists(output_dir):
            pass
        else:
            os.mkdir(output_dir)

        for rlayer in input_rasters:

            out_path = os.path.join(output_dir, rlayer.name() + ".tif")
            out_path_comp = os.path.join(output_dir, rlayer.name()+ "_comp.tif")
            raster_folder = os.path.join(output_dir, rlayer.name())
            crs = rlayer.crs()

            out_opt = 'TILED=YES|BLOCKXSIZE=256|BLOCKYSIZE=256|COMPRESS=DEFLATE'
            composites = ["rgb", "cir", "weeds", "10_7_3", "9_7_2", "9_6_1"]

            if rlayer.name() in composites:
                processing.run("gdal:translate", {
                    'INPUT':rlayer.dataProvider().dataSourceUri(),'TARGET_CRS':'EPSG:3857',
                    'COPY_SUBDATASETS':False, 'OPTIONS': out_opt,
                    'DATA_TYPE':0, 'OUTPUT':out_path_comp})
                    # 'NODATA':nodata

            else:

                #determines nessc. output files and folders

                extent = rlayer.extent()
                width, height = rlayer.width(), rlayer.height()
                renderer = rlayer.renderer()
                provider = rlayer.dataProvider()
                
                #obtains key raster info from qgis
                pipe = QgsRasterPipe()
                pipe.set(provider.clone())
                pipe.set(renderer.clone())
                file_writer = QgsRasterFileWriter(out_path)
                file_writer.writeRaster(pipe, width, height, extent, crs)
                #process allows direct exporting of rasters to images

                #     nodata = 0
                #     #0
                #     #ensures black at edges of raster are removed if composite
                # else:
                #     nodata = None

                
                #Possibly add option to determine compression and tiling

                processing.run("gdal:translate", {
                    'INPUT':out_path, 'TARGET_CRS':None,
                    'COPY_SUBDATASETS':False, 'OPTIONS': out_opt, #'EXTRA':'-mask 4',
                    'DATA_TYPE':0, 'OUTPUT':out_path_comp})
                    # 'NODATA':nodata
                #Corrects nodata value and applies tiling and compression to img

            if os.path.exists(raster_folder):
                pass
            else:
                os.mkdir(raster_folder)
            nodata =0
            processing.run("gdal:gdal2tiles", {
                'INPUT':out_path_comp, 'PROFILE':0, 'ZOOM':'14-21', 'VIEWER':4,
                'TITLE':'', 'COPYRIGHT':'', 'RESAMPLING':0, 'SOURCE_CRS':crs,
                'NODATA':nodata, 'URL':'', 'GOOGLE_KEY':'', 'BING_KEY':'',
                'RESUME':True, 'KML':False, 'NO_KML':False,
                'OUTPUT':raster_folder})
            #Tiles compressed image

            if remove_files == True:
                if os.path.exists(out_path):
                    os.remove(out_path)
                if os.path.exists(out_path_comp):
                    os.remove(out_path_comp)
                #deletes raster and compressed raster
            else:
                pass

        #
        return {self.OUTPUT: output_dir}

        
    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return '2) Web tile export'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(self.name())

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(self.groupId())

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return ''

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ExportMicaSenseAlgorithm()

    def flags(self):
        return super().flags() | QgsProcessingAlgorithm.FlagNoThreading | QgsProcessingAlgorithm.FlagCanCancel | QgsProcessingAlgorithm.FlagSupportsBatch 
        #needed for external dependencies, not thread safe, no background running




# my_proj =  self.parameterAsCrs(parameters,
#                         self.PROJECTION,
#                         context)  
# matrix = self.parameterAsMatrix(parameters,self.TABLE,context)

# bands = matrix[::2]

# try:
#     values = [int(i) for i in matrix if i not in bands]

# except ValueError:
#     raise QgsProcessingException("""Ensure Custom band designations
#                                 are integer values between 1 to 11""")

        # self.addParameter(
#     QgsProcessingParameterMatrix(
#         self.TABLE,
#         self.tr('table matrix'),
#         numberRows = 5,
#         headers = ["Band Name","Position"],
#         defaultValue = ["Coastal Blue - 444","0","Blue - 475","0",
#         "Green - 531","0","Green - 560","0","Red - 650","0","Red - 668","0",
#         "Red Edge - 705","0","Red Edge - 717","0","Red Edge - 740","0",
#         "NIR - 842","0","LWIR/Thermal - 11 μm","0"],
#         hasFixedNumberRows = True))



# proj_param = QgsProcessingParameterCrs(self.PROJECTION,
#                         self.tr("Reproject input data"))
                
# proj_param.setFlags(proj_param.flags() |
#                       QgsProcessingParameterDefinition.FlagAdvanced)

# self.addParameter(proj_param)