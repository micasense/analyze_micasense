

import numpy as np


# def simple_index(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize,
#                  raster_ysize, buf_radius, gt, **kwargs):

#     '''Simple math for NDVI or NDRE ESQUE index calculations'''

#     np.seterr(divide='ignore', invalid='ignore')
#     num = np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
#     den = np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
#     np.divide( num, den, dtype = float, out = out_ar )
#     out_ar[den == 0] = -2.0

# def simple_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
#     np.seterr(divide='ignore', invalid='ignore')
#     np.divide( np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float) , np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float), dtype = float, out = out_ar )
#     out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
#     out_ar[out_ar[:] < 0.45] = -2.0 #remove soil by ndvi value'''





def ndvi_all(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    
    np.seterr(divide='ignore', invalid='ignore')
    num = np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
    den = np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
    np.divide( num, den, dtype = float, out = out_ar )
    out_ar[den == 0] = -2.0

def ndvi_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    np.divide( np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float) , np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float), dtype = float, out = out_ar )
    out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
    out_ar[out_ar[:] < 0.45] = -2.0 #remove soil by ndvi value'''

def ndre_all(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    num = np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
    den = np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float)
    np.divide( num, den, dtype = float, out = out_ar )
    out_ar[den == 0] = -2

def ndre_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    ndvi = np.divide( np.subtract(in_ar[0],in_ar[2], dtype = float) , np.add(in_ar[0],in_ar[2], dtype = float), dtype = float)
    np.divide( np.subtract(in_ar[0],in_ar[1], dtype=float) , np.add(in_ar[0],in_ar[1], dtype=float), dtype = float, out = out_ar )
    out_ar[in_ar[0] < 6553] = -2.0 # remove shadows
    out_ar[ndvi < 0.45] = -2.0     # remove soil'''

def ccci_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    ndvi = np.divide( np.subtract(in_ar[0],in_ar[2], dtype = float) , np.add(in_ar[0],in_ar[2], dtype = float), dtype = float)
    ndre = np.divide( np.subtract(in_ar[0],in_ar[1], dtype = float) , np.add(in_ar[0],in_ar[1], dtype = float), dtype = float)
    np.divide( np.subtract(ndre, ndvi, dtype = float), np.add(ndre, ndvi, dtype = float), out = out_ar)
    out_ar[in_ar[0] < 6553] = -10000.0  # remove shadows
    out_ar[ndvi < 0.45] = -10000.0      # remove soil'''

def lwir_celcius_all(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    np.subtract( np.multiply( in_ar[0], 0.01, dtype = float), 273.150000000, dtype=float, out=out_ar)
    out_ar[out_ar < -270] = -10000

def lwir_celcius_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    ndvi = np.divide( np.subtract(in_ar[0],in_ar[1], dtype = float) , np.add(in_ar[0],in_ar[1], dtype = float), dtype = float)
    np.subtract( np.multiply( in_ar[2], 0.01, dtype = float), 273.15, dtype=float, out=out_ar)
    out_ar[in_ar[0] < 6553] = -10000 # remove shadows
    out_ar[ndvi < 0.45] = -10000     # remove soil'''

def osavi_all(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    num = np.subtract(in_ar[0].astype(float),in_ar[1].astype(float),dtype = float)
    den = np.add(np.add(in_ar[0].astype(float),in_ar[1].astype(float),dtype = float),0.16,dtype = float)
    np.divide(num, den, dtype = float, out = out_ar )
    out_ar[in_ar[0] == 0] = -2.0 # removes no data values
    #out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
    out_ar[den == 0] = -2.0

def osavi_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):

    # [(NIR – red) / (NIR + red + L)] * (1 + L) // common L = 0.16
    np.seterr(divide='ignore', invalid='ignore')
    np.divide( np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float) , np.add(np.add(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float), 0.16, dtype = float), dtype = float, out = out_ar )
    out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
    out_ar[out_ar[:] < 0.45] = -2.0 #remove soil by ndvi value'''

def evi_all(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    G = 2.5
    C1 = 6.0
    C2 = 7.5 
    L = 1.0
    np.seterr(divide='ignore', invalid='ignore')
    num = np.multiply(G, np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float), dtype = float)
    red = np.multiply(in_ar[1], C1, dtype=float)
    blu = np.multiply(in_ar[2], C2, dtype=float)
    den = np.add(in_ar[0], np.add(red, np.add(blu, L, dtype=float), dtype=float), dtype=float)
    np.divide(num, den, dtype = float, out = out_ar )
    out_ar[in_ar[0] == 0] = -2.0 # removes no data values
    #out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
    out_ar[den == 0] = -2.0

def evi_plants(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):

    # EVI
    # return r.G * ((npx - rpx) / (npx + (rpx * r.C1) - (bpx * r.C2) + r.L))
    # G = 2.5, C1 = 6.0, C2 = 7.5, L = 1.0)
    G = 2.5
    C1 = 6.0
    C2 = 7.5 
    L = 1.0
    np.seterr(divide='ignore', invalid='ignore')
    num = np.multiply(G, np.subtract(in_ar[0].astype(float),in_ar[1].astype(float), dtype = float), dtype = float)
    red = np.multiply(in_ar[1], C1, dtype=float)
    blu = np.multiply(in_ar[2], C2, dtype=float)
    den = np.add(in_ar[0], np.add(red, np.add(blu, L, dtype=float), dtype=float), dtype=float)
    np.divide(num, den, dtype = float, out = out_ar )
    out_ar[in_ar[0] < 6553] = -2.0 #remove shadows by NIR reflectance
    out_ar[out_ar[:] < 0.45] = -2.0 #remove soil by ndvi value'''


def mcari(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    a = np.subtract(in_ar[0].astype(float), in_ar[1].astype(float),dtype=float)
    b = np.multiply(0.2,np.subtract(in_ar[0].astype(float) - in_ar[2].astype(float),dtype=float),dtype=float)
    c = np.divide(in_ar[0].astype(float), in_ar[1].astype(float),dtype=float)

    res = np.multiply(np.subtract(a,b),c,dtype=float,out=out_ar)



def pri(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    a = np.subtract(in_ar[0].astype(float) , in_ar[1].astype(float),dtype=float)
    b = np.add(in_ar[0].astype(float) , in_ar[1].astype(float),dtype=float)
    np.divide(a,b,dtype=float,out=out_ar)

ndre * 3

def ci_green(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    np.seterr(divide='ignore', invalid='ignore')
    np.subtract(np.divide(in_ar[0].astype(float) , in_ar[1].astype(float),dtype=float),-1,dtype=float,out=out_ar)


def reip_3(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):

    np.seterr(divide='ignore', invalid='ignore')
    red = in_ar[0].astype(float) 
    re_705= in_ar[1].astype(float) 
    re_740= in_ar[2].astype(float) 
    nir= in_ar[3].astype(float) 

    a= np.add(re_705 , 35,dtype=float)


    b=np.subtract(np.divide(np.add(red,nir),2,dtype=float),re_705,dtype=float)
    c= np.subtract(re_740,re_705,dtype=float)
    d = np.divide(b,c,dtype=float)

    res = np.multiply(a,d,dtype=float,out = out_ar)


def antho(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
    a = np.divide(1,in_ar[0],dtype=float)
    b = np.divide(1,in_ar[1],dtype=float)
    np.subtract(a,b,dtype=float,out=out_ar)