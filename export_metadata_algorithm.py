import processing
import matplotlib.pyplot as plt
import math
import json
from osgeo import gdal
from PyQt5.QtCore import QSettings, QTranslator, qVersion
from PyQt5.QtGui import QIcon, QColor, QIntValidator
from PyQt5.QtWidgets import (QAction, QFileDialog, QDialog, QLineEdit,
                             QDialogButtonBox, QFormLayout, QGroupBox)
import os.path
import numpy as np
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterString,
                       QgsProcessingParameterDefinition,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterCrs,
                       QgsProcessingParameterMultipleLayers,
                       QgsProcessingParameterMatrix)

from qgis.core import (
    QgsRasterLayer,
    QgsProject,
    QgsSingleBandPseudoColorRenderer,
    QgsColorRampShader,
    QgsRasterShader,
    QgsRasterBandStats,
    QgsRasterResampler,
    QgsBilinearRasterResampler,
    QgsCubicRasterResampler,
    QgsContrastEnhancement,
    QgsLayerTreeLayer, QgsRasterPipe, QgsRasterFileWriter,
    QgsCoordinateReferenceSystem, QgsProcessingException)

class ExportMetadataAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector rlayer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    OUTPUT = 'OUTPUT'
    GSD = "GSD"
    TABLE = "TABLE"
    CLEAN = "CLEAN FILES"
    AGL = 'AGL'
    SENSOR = 'SENSOR'
    INFO = 'info'
    DRONE = 'drone'
    PROVIDER = 'provider'
    PROVIDER_LINK = "provider_link"
    VIEWER_LINK = 'viewer_link'
    LINK_TEXT = "link_text"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT,
                self.tr('Output folder')))
        #sets output directory as existing folder direcotry


        info = QgsProcessingParameterString(self.INFO,
                                            "General info on the flight")
        self.addParameter(info)

        self.addParameter(
        QgsProcessingParameterEnum(self.SENSOR,
        self.tr('Choose Sensor'),  
        options=['RedEdge-MX','Altum','DualCamera'],
        optional=False)
        )

        gsd= QgsProcessingParameterNumber(self.GSD,
                        'GSD',
                        QgsProcessingParameterNumber.Double, 8)
                        
  
        gsd.setMetadata({'widget_wrapper':{ 'decimals': 2 }})
        self.addParameter(gsd)

        agl = QgsProcessingParameterNumber(self.AGL,
                        'Above ground altitude of the flight (AGL) in m',
                        QgsProcessingParameterNumber.Double, 120)
                        
  
        agl.setMetadata({'widget_wrapper':{ 'decimals': 0 }})
        self.addParameter(agl)




        drone = QgsProcessingParameterString(self.DRONE,
                                            "Drone / platform used")
        self.addParameter(drone)

        provider = QgsProcessingParameterString(self.PROVIDER,
                                            "Provider of data")
        self.addParameter(provider)

        provider_link = QgsProcessingParameterString(self.PROVIDER_LINK,
                                            "Web link to data provider")
        self.addParameter(provider_link)

        link_text = QgsProcessingParameterString(self.LINK_TEXT,
                                            "Hyperlink text for viewer")
        self.addParameter(link_text)

        viewer_link = QgsProcessingParameterString(self.VIEWER_LINK,
                                            "Viewer web link")
        self.addParameter(viewer_link)


    def processAlgorithm(self, parameters, context, feedback):
        
        """This algorithm takes a symoblised raster(s) from within qgis, exports
        them as an image , compresses said image, then tiles the compressed
        image for web tiling purposes"""


        output_dir = self.parameterAsFileOutput(parameters, 
                                                self.OUTPUT,
                                                context)
        my_gsd = self.parameterAsDouble(parameters,
                                        self.GSD,
                                        context)

        my_agl = self.parameterAsDouble(parameters,
                                    self.AGL,
                                    context)

        sensor_options =['RedEdge-MX','Altum','DualCamera']

        sensor_idx = self.parameterAsEnum(parameters, self.SENSOR, context)
        sensor = sensor_options[sensor_idx]
     

        info = self.parameterAsString(parameters, self.INFO,context)
        drone = self.parameterAsString(parameters, self.DRONE,context)    
        provider = self.parameterAsString(parameters, self.PROVIDER,context)   
        provider_link = self.parameterAsString(parameters, self.PROVIDER_LINK,context)  
        viewer_link = self.parameterAsString(parameters, self.VIEWER_LINK,context)  
        link_text =  self.parameterAsString(parameters, self.LINK_TEXT,context)                    



        data_dict = {"info":info,"sensor":sensor,"gsd":str(my_gsd)+"cm",
                           "AGL":str(int(my_agl))+"m","drone":drone,"provider":provider,
                           "link":provider_link,"link_text":link_text,
                           "viewer_link":viewer_link}  

        out_path = os.path.join(output_dir,"info.json")
        with open(out_path, 'w') as json_file:
            json.dump(data_dict, json_file)

        return {self.OUTPUT: output_dir}

        
    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return '3) Export metadata'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(self.name())

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(self.groupId())

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return ''

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ExportMetadataAlgorithm()

    def flags(self):
        return super().flags() | QgsProcessingAlgorithm.FlagNoThreading | QgsProcessingAlgorithm.FlagCanCancel | QgsProcessingAlgorithm.FlagSupportsBatch 
        #needed for external dependencies, not thread safe, no background running


